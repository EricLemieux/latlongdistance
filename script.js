function distance(){
	var R = 6371000;
	var lat1 = 43.948750;
	var lon1 = -78.897597;

	var lat2 = 43.949407;
	var lon2 = -78.897940;

	var rlat1 = toRadians(lat1);
	var rlat2 = toRadians(lat2);

	var deltaLat = toRadians(lat2 - lat1);
	var deltaLon = toRadians(lon2 - lon1);

	var a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) +
					Math.cos(rlat1) * Math.cos(rlat2) *
					Math.sin(deltaLon/2) * Math.sin(deltaLon/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

	var d = R * c;

	return d;
}

function toRadians(deg){
	return deg * (Math.PI/180);
}

console.log(distance());
